import pytest
from main import countUnevenChars

def one_uneven_char():
    assert 1 == countUnevenChars("aabxbcc")

def no_uneven_chars():
    assert 0 == countUnevenChars("aabbcc")

def no_uneven_chars_empty_str():
    assert 0 == countUnevenChars("")

def three_only_uneven_chars():
    assert 3 == countUnevenChars("abc")

def three_mixed_uneven_chars():
    assert 3 == countUnevenChars("xxArrBggC")
