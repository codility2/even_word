from collections import Counter

def countUnevenChars(word):
    return sum([v % 2 for _, v in Counter(word).items()])
